package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import scanners.ChapterListGrabber;
import scanners.ImageDownloader;
import scanners.ImageUrlGrabber;

public class CaseyMangaDownloader {

	public static String mangaName;
	private static ArrayList<String> mangaList = new ArrayList<String>();
	private static ArrayList<String> urlList = new ArrayList<String>();

	public static void main(String[] args) throws IOException,
			InterruptedException {

		// Initialization logic
		// Enter manga and landing page combos
		mangaList.add("Hakaijuu");
		urlList.add("http://bato.to/comic/_/comics/hakaijuu-r4209");

		mangaList.add("Mob Psycho 100");
		urlList.add("http://bato.to/comic/_/comics/mob-psycho-100-r10553");

		//mangaList.add("Tokyo Ghoul");
		//urlList.add("http://bato.to/comic/_/comics/tokyo-ghoul-r3056");

		mangaList.add("Tokyo Ghoul re");
		urlList.add("http://bato.to/comic/_/comics/tokyo-ghoulre-r13502");

		mangaList.add("Dorohedoro");
		urlList.add("http://bato.to/comic/_/comics/dorohedoro-r2379");

		mangaList.add("Sprite");
		urlList.add("http://bato.to/comic/_/comics/sprite-r470");
		
		mangaList.add("D Gray Man");
		urlList.add("http://bato.to/comic/_/comics/dgray-man-r1751");
		
		//mangaList.add("I Am a Hero");
		//urlList.add("http://bato.to/comic/_/comics/i-am-a-hero-r907");

		for (int i = 0; i < mangaList.size(); i++) {
			mangaName = mangaList.get(i);
			downloadMangaChapters(mangaList.get(i), urlList.get(i));
		}

		System.out.println("\nAll Done");
	}

	public static void downloadMangaChapters(String mangaName,
			String landingPage) throws IOException, InterruptedException {
		createDirectory(Settings.mangaDir + mangaName + "/");
		ChapterListGrabber grabber = new ChapterListGrabber();
		ImageUrlGrabber pageGrabber = new ImageUrlGrabber();

		// Get chapters
		// TODO: Identify comic pages via Search, and better language identifier
		// (something besides english)
		ArrayList<String> chapterList = grabber.getChapterUrls(landingPage,
				"row lang_English chapter_row");

		System.out.println("Found " + chapterList.size() + " chapters!\n");
		Thread.sleep(2000);

		// For each chapter found, grab the image URLs and save them
		for (int i = 0; i < chapterList.size(); i++) {

			String chapterDir = String.format("%04d", i + 1) + " - "
					+ getChapterName(chapterList.get(i));

			if (!createDirectory(Settings.mangaDir + mangaName + "/"
					+ chapterDir)) {
				System.out.println("    ALREADY EXISTS: " + chapterDir);
				if (!Settings.validateDownloadedFiles) {
					continue;
				} else {
					System.out.println(" validating files...");
				}
			}
			System.out.println("  CHAPTER: " + chapterDir);
			ArrayList<String> pageList = pageGrabber.getImageUrls(
					chapterList.get(i), 0);

			ExecutorService imageDownloadPool = Executors
					.newFixedThreadPool(Settings.numThreads);

			for (int j = 0; j < pageList.size(); j++) {
				imageDownloadPool.submit(new ImageDownloader(pageList.get(j),
						j + 1, chapterDir));
			}

			imageDownloadPool.shutdown();
			imageDownloadPool.awaitTermination(Long.MAX_VALUE,
					TimeUnit.MILLISECONDS);
		}
	}

	/**
	 * Creates a directory if it doesn't exist
	 */
	public static boolean createDirectory(String dirName) {
		File theDir = new File(dirName);
		if (!theDir.exists()) {
			try {
				theDir.mkdir();
				return true;
			} catch (SecurityException se) {
				se.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * Isolate chapter name from the chapter URL, also remove underscores
	 * 
	 * @param chapterUrl
	 * @return
	 */
	public static String getChapterName(String chapterUrl) {
		String output = chapterUrl;
		int slashLocation = 0;

		for (int i = output.length() - 1; i > 0; i--) {
			if (output.charAt(i) == '/') {
				slashLocation = i;
				break;
			}
		}

		String tmp = "";
		for (int i = slashLocation + 1; i < output.length(); i++) {
			tmp += output.charAt(i);
		}
		output = tmp;

		return output.replaceAll("_", " ");
	}
}
