package main;

public class Settings {
	// Download preferences
	public static String mangaDir = "C:/Users/Casey/Pictures/Manga/";
	public static boolean validateDownloadedFiles = false;
	public static int numThreads = 4; // threads for simultaneous page download
	public static int retryInterval = 20000; // 20 seconds
	public static int retryCount = 10;
}