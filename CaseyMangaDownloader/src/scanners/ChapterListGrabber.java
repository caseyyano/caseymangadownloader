package scanners;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Grabs all of the chapter elements given the comic's page URL and a language
 * specified. Finds all the chapter URLs by the naming of the table class and
 * then matching the value of the URL (strips out forum/contributor links)
 */
public class ChapterListGrabber {

	public ArrayList<String> getChapterUrls(String comicURL, String language)
			throws IOException, InterruptedException {

		ArrayList<String> output = new ArrayList<String>();

		// Go to the comic detail page URL
		Document doc = ConnectionHelper.connectToUrl(comicURL, 0);

		Elements chapterElements = doc.getElementsByAttributeValueContaining(
				"class", language);

		//
		for (int i = 0; i < chapterElements.size(); i++) {
			output.add(chapterElements
					.get(i)
					.getElementsByAttributeValueStarting("href",
							"http://bato.to/read/_/").get(0).attr("href"));
		}

		Collections.reverse(output);
		return output;
	}
}
