package scanners;

import java.io.IOException;

import main.Settings;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Contains helper method to help connect to a URL. Will automatically retry
 */
public class ConnectionHelper {

	public static Document connectToUrl(String url, int attempt)
			throws InterruptedException {
		if (attempt == Settings.retryCount - 1) {
			System.out.println("Rejected " + Settings.retryCount
					+ " times, give up for tonight");
			return null;
		}
		try {
			return Jsoup.connect(url).get();
		} catch (IOException e) {
			System.out.println("Connection to " + url + " rejected, retry in "
					+ Settings.retryInterval / 1000 + " seconds");
			Thread.sleep(Settings.retryInterval);
			return connectToUrl(url, attempt + 1);
		}
	}
}
