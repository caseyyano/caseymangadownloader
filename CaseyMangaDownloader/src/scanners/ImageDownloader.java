package scanners;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import main.CaseyMangaDownloader;
import main.Settings;

/**
 * Given an image URL, ImageDownloader will attempt to save the image at the URL
 * specified to your preferred directory. If the URL is not available, it will
 * attempt the same URL but flip the content type to JPG/PNG. Will skip on fail
 */
public class ImageDownloader implements Runnable {

	private String imageUrl;
	private String chapterName;
	private int pageCount;

	public ImageDownloader(String imageUrl, int pageCount, String chapterName) {
		this.imageUrl = imageUrl;
		this.pageCount = pageCount;
		this.chapterName = chapterName;
	}

	/**
	 * Performs a series of operations to download, rename, and saving the image
	 * from an image URL
	 * 
	 * @param imageUrl
	 * @param firstAttempt
	 * @param pageCount
	 * @param chapterName
	 * @throws IOException
	 */
	public void getImage(String imageUrl, int pageCount, String chapterName)
			throws IOException {

		String type = imageUrl.substring(imageUrl.length() - 3,
				imageUrl.length());

		String saveFile = Settings.mangaDir + CaseyMangaDownloader.mangaName
				+ '/' + chapterName + '/' + String.format("%03d", pageCount)
				+ '.' + type;

		File f = new File(saveFile);

		if (!f.exists()) {
			InputStream in = null;

			// Connect to image URL
			try {
				in = new BufferedInputStream(new URL(imageUrl).openStream());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Save image
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			saveImage(out.toByteArray(), pageCount, f, type);

			// Cleanup
			out.close();
			in.close();
		}
	}

	/**
	 * Renames the file to be the page count and saves the image to your
	 * mangaDirectory/mangaName/chapterName directory
	 * 
	 * @param response
	 * @param pageCount
	 * @param chapterName
	 * @param type
	 * @throws IOException
	 */
	private void saveImage(byte[] response, int pageCount, File theFile,
			String type) throws IOException {

		System.out.println("    PAGE: " + chapterName + '/'
				+ String.format("%03d", pageCount) + '.' + type);

		FileOutputStream fos = new FileOutputStream(theFile);

		fos.write(response);
		fos.close();
	}

	// Necessary for multi-threading the image downloading process
	public void run() {
		try {
			getImage(imageUrl, pageCount, chapterName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
