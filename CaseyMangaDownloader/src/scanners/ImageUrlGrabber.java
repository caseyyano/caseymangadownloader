package scanners;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Identifies the number of pages a chapter has and returns the URLs for each of
 * the images in this chapter. The intent is to then download these URLs in
 * multiple threads through the image downloader
 */
public class ImageUrlGrabber {

	public ArrayList<String> getImageUrls(String chapterURL, int retryCount)
			throws InterruptedException {

		ArrayList<String> output = new ArrayList<String>();

		// Connect to the chapter URL
		Document doc = ConnectionHelper.connectToUrl(chapterURL, 0);

		// Grab the URLs of all of the pages for this chapter
		Elements chapterInfo = doc
				.getElementsByAttributeValueContaining("name", "page_select")
				.get(0).getElementsByAttributeValueContaining("value", "http");

		// Visit each page and get the image URL
		for (int i = 0; i < chapterInfo.size(); i++) {
			Document pageDoc = ConnectionHelper.connectToUrl(chapterInfo.get(i)
					.val(), 0);

			output.add(pageDoc
					.getElementsByAttributeValueStarting("id", "comic_page")
					.get(0).attr("src"));
		}

		return output;
	}
}
